import HttpRestApiError from './rest/HttpRestApiError';
import GraphqlApiError from './graphql/GraphqlApiError';
import ApiError from './ApiError';

export type AnyError = unknown;

export const isNotAuthorizedError = (
  e: HttpRestApiError | GraphqlApiError | ApiError | AnyError,
) => {
  if (e instanceof HttpRestApiError) {
    return e.status === 401;
  }

  if (e instanceof GraphqlApiError) {
    // @ts-ignore
    // const gqlError = filter((e) => e.message.statusCode === 401, e.graphQLErrors);
    // return !!gqlError;
    throw new Error('Not implemented yet');
  }

  return false;
};
