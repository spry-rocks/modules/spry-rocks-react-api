export default class ApiError extends Error {
  constructor(public readonly message: string, public readonly cause?: any) {
    super(message);

    Object.setPrototypeOf(this, ApiError.prototype);
  }
}
