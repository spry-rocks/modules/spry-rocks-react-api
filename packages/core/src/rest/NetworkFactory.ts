/* eslint-disable class-methods-use-this */
import {INetworkFactory} from './INetworkFactory';
import {Network} from './Network';

export class NetworkFactory implements INetworkFactory {
  createNetwork(baseURL: string, headers: object, getHeaders: () => object) {
    return new Network(baseURL, headers, getHeaders);
  }
}
